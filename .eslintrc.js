module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    "semi": [2, "always"],//语句强制分号结尾
    'no-tabs': 'off',
    "no-mixed-spaces-and-tabs": [0, false],//禁止混用tab和空格
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
