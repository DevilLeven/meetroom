// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import '@babel/polyfill';
import Vue from 'vue';
import App from './App';
import router from './router';
import Element from 'element-ui';
import Config from './config';
import store from './store';
// import 'iview/dist/styles/iview.css';
import 'element-ui/lib/theme-chalk/index.css';
import ssoLogin from './components/ssoLoing';
import './assets/icons/iconfont.css'; // 阿里巴巴矢量图标 可自定义替换

Vue.use(Element, {
  size: Config.system.ElementSize
});
Vue.use(ssoLogin);
Vue.config.productionTip = false;

/* eslint-disable no-new */
let APPVue = new Vue({
  'el': '#app',
  router,
  store,
  'components': { App },
  'template': '<App/>'
});

export default APPVue;
