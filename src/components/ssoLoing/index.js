import SSOLogin from './ssoLogin';
import router from '../../router';
export default {
  install (Vue) {
    const SsoLogin = Vue.extend(SSOLogin);

    let login = null;

    router.addRoutes([{
      name: 'ssologin',
      path: '/ssologin',
      component: () => import('./loginCookies.vue')
    }]);
    function show (URL) {
      if (!login) {
        login = new SsoLogin();
        // 手动创建一个未挂载的实例
        login.$mount();
        // 挂载
        document.querySelector('body').appendChild(login.$el);
      }
      login.show(URL);
    }
    function Redirect (URL) {
      if (!login) {
        login = new SsoLogin();
        // 手动创建一个未挂载的实例
        login.$mount();
        // 挂载
        document.querySelector('body').appendChild(login.$el);
      }

      login.Redirect(URL);
    }
    let sso = {
      showModal: show,
      Redirect: Redirect
    };

    Vue.prototype.$SSO = sso;
  }
};
