import {otherRouter, appRouter} from '@/router/router';
import EventBus from '../../libs/eventBus';
const app = {
  state: {
    menuWidth: 200,
    cachePage: [],
    menuList: [],
    pageOpenedList: [],
    tagsList: [],
    routers: [
      otherRouter,
      ...appRouter
    ],
    dontCache: ['text-editor', 'artical-publish'] // 在这里定义你不想要缓存的页面的name属性值(参见路由配置router.js)
  },
  mutations: {
    updateMenulist (state) {
      let menuList = [];

      appRouter.forEach((item) => {
        if (item.children.length === 1) {
          menuList.push(item);
        } else {
          let len = menuList.push(item);
          let childrenArr = [];

          childrenArr = item.children.filter(child => {
            return child;
          });
          menuList[len - 1].children = childrenArr;
        }
      });

      state.menuList = menuList;
    },
    setCurrentPageName (state, name) {
      state.currentPageName = name;
    },
    setCurrentPath (state, pathArr) {
      state.currentPath = pathArr;
    },
    removeTag (state, name) {
      state.pageOpenedList.map((item, index) => {
        if (item.name === name) {
          state.pageOpenedList.splice(index, 1);
        }
      });
    },
    pageOpenedList (state, get) {
      let openedPage = state.pageOpenedList[get.index];

      if (get.argu) {
        openedPage.argu = get.argu;
      }
      if (get.query) {
        openedPage.query = get.query;
      }
      state.pageOpenedList.splice(get.index, 1, openedPage);
      localStorage.pageOpenedList = JSON.stringify(state.pageOpenedList);
    },
    clearAllTags (state) {
      state.pageOpenedList = [];
      state.cachePage.length = 0;
      localStorage.pageOpenedList = JSON.stringify(state.pageOpenedList);
    },
    clearOtherTags (state, vm) {
      let currentName = vm.$route.name;
      let currentIndex = 0;

      state.pageOpenedList.forEach((item, index) => {
        if (item.name === currentName) {
          currentIndex = index;
        }
      });
      if (currentIndex !== 0) {
        state.pageOpenedList.splice(currentIndex + 1);
        state.pageOpenedList.splice(0, currentIndex);
      } else {
        state.pageOpenedList.splice(1);
      }
      let newCachepage = state.cachePage.filter(item => {
        return item === currentName;
      });

      state.cachePage = newCachepage;
      localStorage.pageOpenedList = JSON.stringify(state.pageOpenedList);
    },
    closePage (state, name) {
      state.cachePage.forEach((item, index) => {
        if (item === name) {
          state.cachePage.splice(index, 1);
        }
      });
    },
    setOpenedList (state) {
      state.pageOpenedList = localStorage.pageOpenedList ? JSON.parse(localStorage.pageOpenedList) : [];
    },
    increateTag (state, tagObj) {
      if (state.dontCache.indexOf(tagObj.name) < 0) {
        state.cachePage.push(tagObj.name);
        localStorage.cachePage = JSON.stringify(state.cachePage);
      }
    },
    increateOpened (state, newpage) {
      state.pageOpenedList.push(newpage);
      if (state.dontCache.indexOf(newpage.name) < 0) {
        state.cachePage.push(newpage.name);
        localStorage.cachePage = JSON.stringify(state.cachePage);
      }
      localStorage.pageOpenedList = JSON.stringify(state.pageOpenedList);
    },
    initCachepage (state) {
      if (localStorage.cachePage) {
        state.cachePage = JSON.parse(localStorage.cachePage);
        EventBus.$on('clearCachePage', (pagename) => {
          if (state.cachePage.length) {
            let index = state.cachePage.indexOf(pagename);

            if (index >= 0) {
              state.cachePage.splice(index, 1);
            }
          }
        });
      }
    }
  }
};

export default app;
