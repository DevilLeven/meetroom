import API from '@/API';
// import PopperJS from 'popper.js';
import PopperJS from 'element-ui/src/utils/popper.js';
export default {
  methods: {
    addfavorite (item) {
      // 添加收藏
      API.addfavorite(item.RoomID).then((res) => {
        let json = res.data;

        if (json.status === '200') {
          item.IsFavorite = true;
        }
      }).catch(() => {

      });
    },
    removefavorite (item, callBack) {
      // 添加收藏
      API.removefavorite(item.RoomID).then((res) => {
        let json = res.data;
        if (json.status === '200') {
          if (callBack) {
            callBack(true);
          }
          item.IsFavorite = false;
        } else {
          this.$notify({
            title: '失败',
            message: json.err,
            type: 'success'
          });
          if (callBack) {
            callBack(false);
          }
        }
      }).catch(() => {
        if (callBack) {
          callBack(false);
        }
      });
    },
    Canlcereserver ({ReserveID, callback, index, type}) {
      // 取消预约
      API.cancelreserver(ReserveID).then((res) => {
        if (callback) {
          callback();
        }
        let json = res.data;
        if (json.status === '200') {
          this.$notify({
            title: '成功',
            message: type === 'complete' ? '结束成功' : '取消成功',
            type: 'success'
          });
          if (this.roomList) {
            this.roomList[index.row].ReservelList.splice(index.num, 1);
          }
          if (this.showdestineModelToggle) {
            this.showdestineModelToggle();
          }
        } else {
          this.$notify({
            title: '失败',
            message: json.err,
            type: 'error'
          });
        }
      }).catch(() => {
        if (callback) {
          callback();
        }
      });
    },
    showdestineModel (ReserveID, index, event) {
      // 显示预定详情弹窗
      if (this.destineModel.show && this.destineModel.ReserveID === ReserveID) {
        this.showdestineModelToggle();
        return;
      } else if (this.destineModel.show) {
        this.showdestineModelToggle();
      }

      this.$nextTick(() => {
        this.destineModel.show = true;
        this.destineModel.ReserveID = ReserveID;
        this.destineModel.index = index;
        this.destineModel.reference = event.target;
        this.destineModel.popper = this.$refs.destinemodel;
        document.body.appendChild(this.destineModel.popper);
        this.view.destinePopper = new PopperJS(this.destineModel.reference, this.destineModel.popper, {
          placement: 'bottom',
          positionFixed: false,
          gpuAcceleration: false
        });

        this.destineModel.reference.addEventListener('click', this.showdestineModelToggle, false);
        document.addEventListener('click', this.destineModelDocumentClick, false);
      });
    },
    showdestineModelToggle () {
      if (this.view.destinePopper) {
        this.view.destinePopper.destroy();
        document.body.removeChild(this.destineModel.popper);
        this.view.destinePopper = null;
      }
      this.destineModel.popper = null;
      this.destineModel.ReserveID = null;
      this.destineModel.show = false;
      if (this.destineModel.reference) {
        this.destineModel.reference.removeEventListener('click', this.showdestineModelToggle, false);
      }
      document.removeEventListener('click', this.destineModelDocumentClick, false);
    },
    destineModelDocumentClick (e) {
      if (!this.destineModel.popper.contains(e.target) && !this.destineModel.reference.contains(e.target)) {
        this.showdestineModelToggle();
      }
    },
    updatedestineModelPopper () {
      if (this.view.destinePopper) {
        this.view.destinePopper.update();
      }
    },
    showAdminReserverTip (room) {
      // 管理员预定的会议室予以提示
      this.$confirm(`该会议室限管理员可预订。如需预订请联系:${room.AdminName},邮箱为：${room.AdminEmail}`, '提示', {
        confirmButtonText: '确定',
        showCancelButton: false,
        type: 'warning'
      }).then(() => {
      }).catch(() => {
      });
    }
  }
};
