import {mapState} from 'vuex';
import API from '@/API';
export default {
  data () {
    return {
      Locationdoing: false
    };
  },
  computed: {
    ...mapState({
      user: state => state.user,
      Locations: state => state.Locations
    })
  },
  methods: {
    initLocations () {
      if (!this.Locations.length && !this.Locationdoing) {
        this.Locationdoing = true;
        this.$nextTick(() => {
          API.getregionList().then((res) => {
            let json = res.data;
            if (json.status === '200') {
              this.Locationdoing = false;
              if (json.data) {
                let array = [];
                json.data.forEach((item) => {
                  let map = {
                    value: item.RegionID,
                    label: item.FullName,
                    children: []
                  };
                  array.push(map);
                });
                this.$store.commit('setLocations', array);
              }
            }
          }).catch(() => {
            this.Locationdoing = false;
          });
        });
      }
    },
    LocationChange (val) {
      if (val.length === 1) {
        let choose = val[0];
        let obj = {};
        if (!this.Locations) {
          setTimeout(() => {
            this.LocationChange(val);
          }, 50);
          return;
        }
        this.Locations.forEach((item, ind) => {
          if (item.value === choose) {
            obj = item;
          }
        });
        if (!obj.children) {
          setTimeout(() => {
            this.LocationChange(val);
          }, 50);
          return;
        }
        if (!obj.children.length) {
          API.getfloorList(choose).then((res) => {
            let json = res.data;
            if (json.status === '200') {
              if (json.data) {
                let array = [];
                json.data.forEach((item) => {
                  let map = {
                    value: item.FloorID,
                    label: item.FloorName
                  };
                  array.push(map);
                });
                obj.children = array;
              }
            }
          }).catch(() => {
          });
        }
      }
    },
    behaviormodify (val) {
      API.behaviormodify(val).then((res) => {
        let json = res.data;
        if (json.status === '200') {
          this.user.Behavior = val;
          this.$store.commit('setUser', this.user);
        }
      }).catch(() => {
      });
    }
  },
  mounted () {
  }
};
