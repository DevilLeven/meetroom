import {parseTime} from '../../libs';
export default {
  methods: {
    meetingTime (timestramp, endtime) {
      // 判断是否 跨天
      let startday = parseTime(new Date(timestramp), '{y}-{m}-{d}');
      let endday = parseTime(new Date(endtime), '{y}-{m}-{d}');
      if (startday !== endday) {
        let start = parseTime(new Date(timestramp), '{y}-{m}-{d} {h}:{i}');
        let end = parseTime(new Date(endtime), '{y}-{m}-{d} {h}:{i}');
        return start + ' - ' + end;
      } else {
        let start = parseTime(new Date(timestramp), '{y}-{m}-{d} {h}:{i}');
        let end = parseTime(new Date(endtime), '{h}:{i}');
        return start + ' - ' + end;
      }
    },
    getReserverStatus (val) {
      let map = {
        0: '待审批',
        2: '提前结束',
        3: '已取消',
        6: '预定成功'
      };
      return map[val];
    },
    getReserverType (val) {
      let map = {
        0: '当天预定',
        1: '跨天预定'
      };
      return map[val];
    },
    getRoomStatus (val) {
      let map = {
        0: '正常',
        1: '维护中',
        2: '停用'
      };
      return map[val];
    },
    getApprovalType (val) {
      let map = {
        0: '无需审批',
        1: '管理员审批',
        2: '管理员预订'
      };
      return map[val];
    },
    getPredefinedmode (val) {
      let map = {
        0: '全员可预订',
        1: '需管理员审批',
        2: '限管理员预订'
      };
      return map[val];
    },
    getApprovalResult (val) {
      let map = {
        0: '待审批',
        1: '已同意',
        2: '已拒绝',
        3: '已取消'
      };
      return map[val];
    }
  }
};
