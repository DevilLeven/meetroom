import {Cascader} from 'element-ui';
export default {
  extends: Cascader,
  methods: {
    handlePick (value, close = true) {
      if (value.length === 1) {
        // 显示下一级
        this.$emit('showNext', value);
      } else {
        this.currentValue = value;
        this.$emit('input', value);
        this.$emit('change', value);
        if (close) {
          this.menuVisible = false;
        } else {
          this.$nextTick(this.updatePopper);
        }
      }
    },
    handleClickoutside (pickFinished = false) {
      if (pickFinished) {
        this.menu.expandTrigger = 'click';
        this.currentValue = this.menu.activeValue;
        this.$emit('input', this.menu.activeValue);
        this.$emit('change', this.menu.activeValue);
        setTimeout(() => {
          this.menu.expandTrigger = 'hover';
        }, 100);
      }
      if (this.menuVisible && !pickFinished) {
        this.needFocus = false;
      }
      this.menuVisible = false;
    }
  }
};
