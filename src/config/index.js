export default {
  system: {
    ENV: process.env.NODE_ENV,
    baseURL: process.env.NODE_ENV === 'development' ? '/api' : '/api',
    ElementSize: 'small',
    Title: 'Admin',
    SSOModalUrl: 'http://sso.ruijie.com.cn/loginframe.php?URL=',
    SSORedirectlUrl: 'http://sso.ruijie.com.cn/login.php?URL='
  }
};

export const MeetingTable = {
  Daily: {
    times: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
  },
  CrossDay: {
    taleLength: 20,
    dayCell: {
      begin: 8, // 每一天 8点开始 13点为中间 21点为最后
      middle: 13,
      end: 21
    },
    notallowtip: '超出可预订时间范围'
  },
  theme: [{'value': '产品发布会'}, {'value': '周报分享'}, {'value': '项目启动会'}, {'value': '部门年会'}]
};
