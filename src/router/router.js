import Main from '@/views/main.vue';
import myCommon from '@/views/user/myCommon.vue';
// 不作为Main组件的子页面展示的页面单独写，如下
export const loginRouter = {
  path: '/login',
  name: 'login',
  meta: {
    title: 'Login - 登录'
  },
  component: () => import('@/views/login.vue')
};
export const mainRouter = {
  path: '/',
  name: 'main',
  redirect: '/Reserve/Visualization',
  meta: {
    title: 'main'
  },
  component: () => import('@/views/main.vue')
};
// 作为Main组件的子页面展示但是不在左侧菜单显示的路由写在otherRouter里
export const otherRouter = {
  path: '/',
  name: 'otherRouter',
  component: myCommon,
  children: [
    { path: '/my/myCollection',
      title: '我的收藏',
      name: 'myCollection',
      component: () => import('@/views/user/myCollection.vue'),
      meta: {
        title: '我的收藏'
      }
    }
  ]
};
export const errorPage = [
  {
    path: '*',
    redirect: '/404'
  },
  {
    path: '/404',
    name: '404',
    title: '404',
    component: () => import('@/views/errorPage/404.vue')
  },
  {
    path: '/test',
    name: 'test',
    title: 'test',
    component: () => import('@/views/test.vue')
  },
  {
    path: '/403',
    name: '403',
    title: '403',
    component: () => import('@/views/errorPage/403.vue')
  }
];
export const appRouter = [
  { // a.第一种情况：只有一级菜单
    path: '/Reserve',
    name: 'Visualization',
    title: '可视化预订',
    component: Main,
    children: [ // 要显示在右侧区域的页面必须作为children来添加
      { path: 'Visualization',
        title: '可视化预订',
        name: 'Visualization_index',
        component: () => import('@/views/Reserve/Visualization.vue'),
        meta: {
          title: '可视化预订'
        }
      }
    ]
  },
  { // a.第一种情况：只有一级菜单
    path: '/my',
    redirect: '/my/myBooked', // 选填，如果不填也会跳转到这个路径
    name: 'myBooked',
    title: '',
    component: myCommon,
    children: [
      { path: 'myBooked',
        title: '我的预订',
        name: 'myBooked-index',
        component: () => import('@/views/user/myBooked.vue'),
        meta: {
          title: '我的预订'
        }
      }
    ]
  }
  // { // a.第一种情况：只有一级菜单
  //   path: '/my',
  //   name: 'NeedTodo',
  //   title: '',
  //   component: myCommon,
  //   children: [
  //     { path: 'myNeedTodo',
  //       title: '待审批',
  //       name: 'myNeedTodo',
  //       component: () => import('@/views/user/needTodo.vue'),
  //       meta: {
  //         title: '待审批',
  //         authority: [1] // 1 管理员
  //       }
  //     }
  //   ]
  // }
];
// 所有上面定义的路由都要写在下面的routers里
export const routers = [
  loginRouter,
  mainRouter,
  otherRouter,
  ...appRouter,
  ...errorPage
];
