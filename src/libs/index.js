
export function title (val) {
  if (val) {
    window.document.title = val;
  }
}

export function openNewPage (vm, to, argu, query) {
  let pageOpenedList = vm.$store.state.app.pageOpenedList;
  let openedPageLen = pageOpenedList.length;
  let i = 0;
  let tagHasOpened = false;

  while (i < openedPageLen) {
    if (to.name === pageOpenedList[i].name) { // 页面已经打开
      vm.$store.commit('pageOpenedList', {
        index: i,
        argu: argu,
        query: query
      });
      tagHasOpened = true;
      break;
    }
    i++;
  }
  if (!tagHasOpened) {
    let currentPathObj = vm.$store.state.app.routers.filter(item => {
      if (item.children.length && item.children.length <= 1) {
        return item.children[0].name === to.name;
      }

      let j = 0;
      let childArr = item.children;
      let len = childArr.length;

      while (j < len) {
        if (childArr[j].name === to.name) {
          return true;
        }
        j++;
      }
      return false;
    })[0];

    if (currentPathObj) {
      let childObj = currentPathObj.children.filter((child) => {
        return child.name === to.name;
      })[0];
      let page = {
        title: childObj.title,
        name: childObj.name,
        path: childObj.path,
        argu: to.params,
        query: to.query
      };

      vm.$store.commit('increateOpened', page);
    }
  }
  vm.$store.commit('setCurrentPageName', name);
}
export function parseTime (time, cFormat) {
  if (arguments.length === 0) {
    return null;
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}';
  let date;

  if (typeof time === 'object') {
    date = time;
  } else if ((String(time)).length === 10) {
    let timestamp = Number(time) * 1000;

    date = new Date(timestamp);
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  };
  const timeStr = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key];

    if (key === 'a') {
      return ['日', '一', '二', '三', '四', '五', '六'][value];
    }
    if (result.length > 0 && value < 10) {
      value = '0' + value;
    }
    return value || 0;
  });

  return timeStr;
}
export function getUTCTime (time) {
  let date = new Date();
  let localOffset = date.getTimezoneOffset() * 60000;
  if (typeof time === 'number') {
    return time + localOffset;
  } else {
    let newdate = new Date(time.getTime() + localOffset);
    return newdate;
  }
}
export function parseLocalTime (time, cFormat) {
  let date = new Date();
  let localOffset = date.getTimezoneOffset() * 60000;
  let newdate = new Date(time.getTime() - localOffset);
  return parseTime(newdate, cFormat);
}
export function formatTime (timeData, option) {
  let time = Number(timeData) * 1000;
  const d = new Date(time);
  const now = Date.now();

  const diff = (now - d) / 1000;

  if (diff < 30) {
    return '刚刚';
  } else if (diff < 3600) { // less 1 hour
    return Math.ceil(diff / 60) + '分钟前';
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前';
  } else if (diff < 3600 * 24 * 2) {
    return '1天前';
  }
  if (option) {
    return parseTime(time, option);
  }
  return d.getMonth() + 1 + '月' + d.getDate() + '日' + d.getHours() + '时' + d.getMinutes() + '分';
}
export function TimeLength (start, end) {
  let text = '';
  let Ml = end - start;
  let H = Ml / (60 * 60000);
  if (H === 0.25) {
    return '';
  }
  if (parseInt(H) > 0) {
    text += parseInt(H) + '小时';
  }
  if ((H - parseInt(H)) > 0) {
    text += (H - parseInt(H)) * 60 + '分钟';
  }

  return text;
}
export function isBJMTC () {
  let date = new Date();
  let offect = date.getTimezoneOffset();
  if (offect === -480) {
    return true;
  }
  return false;
}

/**
 * @param org 原始时间戳
 * @param day 相距天
 * @param hours 小时
 * @param mi 分钟
 * @returns {number} 计算后时间戳
 */
export function lengthTimestramp (org, day, hours, mi) {
  let times = 0 + org;
  if (day) {
    times += day * 24 * 60 * 60 * 1000;
  }
  if (hours) {
    times += hours * 60 * 60 * 1000;
  }
  if (mi) {
    times += mi * 60 * 1000;
  }
  return times;
}

export function merge (target) {
  for (let i = 1, j = arguments.length; i < j; i++) {
    let source = arguments[i] || {};

    for (let prop in source) {
      if (source.hasOwnProperty(prop)) {
        let value = source[prop];

        if (value !== undefined) {
          target[prop] = value;
        }
      }
    }
  }
  return target;
}
