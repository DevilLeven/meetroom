const data = [
  {
    version: '1.0.4',
    time: '2018-08-06',
    contents: [
      '增加更新日志',
      '修复CSS问题导致Element-ui Dialog等弹出组件显示异常',
      '增加自定义阿里巴巴字体图标 iconfont'
    ]
  }

];

export default data;
