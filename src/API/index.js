import axios from 'axios';
import APP from '../main';
import Config from '@/config';
axios.defaults.baseURL = Config.system.baseURL;
// 允许请求发送cookie
axios.defaults.withCredentials = true;
axios.defaults.headers.post['Content-Type'] = 'application/json';
// http请求拦截
axios.interceptors.request.use(config => {
  let ticket = window.localStorage.getItem('TokenID');
  if (ticket) {
    config.headers['Authorization'] = ticket;
  }
  return config;
}, error => {
  return Promise.reject(error);
});
// http响应拦截器
const errorState = ['100000', '100001', '100002', '100003'];
axios.interceptors.response.use(res => {
  if (res.data && res.data.status) {
    if (errorState.indexOf(res.data.status) >= 0) {
      APP.$notify.error({
        title: '错误',
        message: '登录超时,请重新登录'
      });
      // window.localStorage.removeItem('TokenID');
      if (APP.$store.state.user) {
        APP.$SSO.showModal(Config.system.SSOModalUrl);
      }

      return res;
    }
  }

  if (res.data && res.data.status !== '200') {
    if (Config.system.NotificationError) {
      APP.$notify.error({
        title: '错误',
        message: res.data.err
      });
    }
  }
  return res;
}, error => {
  return Promise.reject(error);
});
export default {
  axios: axios,
  dytable: (params) => axios.get('/dytable', params),
  checklogin: (params) => axios.get('user/login/' + params.phplogin),
  tookenlogin: (params) => axios.get('user/get/' + params),
  userAdmin: (params) => axios.get(`user/admin/${params.userId}/${params.password}`),
  getregionList: () => axios.get('room/region/getList'),
  getfloorList: (params) => axios.get(`room/floor/getList/${params}`),
  getreserverroomList: (params) => axios.post('reserver/room/getList', params),
  getMyreserverList: (params) => axios.post('reserver/my/getList', params),
  getMyfavoriteList: (params) => axios.post('room/favorite/getList', params),
  reserverStart: (params) => axios.post('reserver/start', params),
  getreserver: (params) => axios.get(`reserver/get/${params}`),
  cancelreserver: (params) => axios.get(`reserver/cancel/${params}`),
  addfavorite: (params) => axios.get(`favorite/add/${params}`),
  removefavorite: (params) => axios.get(`favorite/remove/${params}`),
  behaviormodify: (params) => axios.post(`behavior/modify`, params),
  gettapprovalLis: () => axios.get('reserver/approval/my/getList'),
  approvalAgree: (params) => axios.post('reserver/approval/agree', params),
  approvalRefuse: (params) => axios.post('reserver/approval/refuse', params),
  getreserverroomDayList: (params) => axios.post('reserver/room/getDayList', params),
  getreserverList: (params) => axios.post('reserver/getList', params)
};
