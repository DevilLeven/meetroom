const CompressionWebpackPlugin = require('compression-webpack-plugin');
const productionGzipExtensions = ['js', 'css'];

module.exports = {
  baseUrl: '/', // vueRouter baseUrl
  outputDir: 'html',
  assetsDir: undefined,
  runtimeCompiler: true,
  productionSourceMap: false,
  parallel: undefined,
  css: {
    extract: false
  },
  chainWebpack: config => {
  },
  configureWebpack: {
    plugins: [
      new CompressionWebpackPlugin({ // gzip 打包压缩
        asset: '[path].gz[query]',
        algorithm: 'gzip',
        test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
        threshold: 10240,
        minRatio: 0.8
      })
    ]
  },
  devServer: {
    port: 9003,
    host: 'my.ruijie.com.cn',
    https: false,
    hot: true,
    hotOnly: false,
    public: 'my.ruijie.com.cn:9003', // sockjs-node host 热更新时的域名，cli3.0的一个小问题
    proxy: {
      // 跨域代理代理
      '/api': {
        target: 'http://172.16.3.78/',
        // target: 'http://172.17.186.31:7100/',
        changeOrigin: true,
        pathRewrite: {
          '/api': '/api'
        }
      }
    },
    // 将 lint 错误输出为编译错误,让浏览器 overlay 同时显示警告和错误
    overlay: {
      warnings: true,
      errors: true
    }
  }
};
