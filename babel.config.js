module.exports = {
  presets: [
    ['@vue/app', {
      'modules': false,
      'useBuiltIns': 'usage',
      'targets': {
        'browsers': ['> 1%', 'last 2 versions', 'not ie <= 8']
      }
    }
    ]
  ]
};
